package com.code.question4.base;

import java.util.List;

public interface Oprator<T> {
    boolean Post(T t);
    boolean Delete(String key);
    List<T> Get(String filed,SearchType type);
    boolean Put(T t);
}
