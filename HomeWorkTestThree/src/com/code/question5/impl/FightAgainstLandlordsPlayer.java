package com.code.question5.impl;

import com.code.question5.base.Identity;
import com.code.question5.base.Player;
import com.code.question5.base.PokeCard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FightAgainstLandlordsPlayer extends Player {
    private List<PokeCard> cards = new ArrayList<>();
    private Identity identity;

    public FightAgainstLandlordsPlayer(String name, long money) {
        this.setName(name);
        this.setMoney(money);
        System.out.println("大家好，我是" + this.getName());
    }

    public List<PokeCard> getCards() {
        return cards;
    }

    /**
     * 发牌每次只发一张牌
     * @param cards
     */
    public void setCards(PokeCard cards) {
        this.cards.add(cards);
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    /**
     * 玩家洗牌
     */
    public void ShuffleCards() {
        Collections.sort(this.cards, (PokeCard p1, PokeCard p2) -> {
            return p1.getOrder() - p2.getOrder();
        });
    }
}
