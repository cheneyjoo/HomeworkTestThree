package com.code.question5.base;

/**
 * 荷官接口，负责发牌与洗牌
 */
public interface ISands {
    void ShuffleCards();

    void DealCards();
}
