package com.code.question5.base;

/**
 * 扑克牌花色，为固定的
 */
public enum Decor {
    Spade("♠"), Diamond("♦"), Clubs("♣"), Hearts("♥");
    private String decorImg;

    Decor(String decorImg) {
        this.decorImg = decorImg;
    }

    public String getDecorImg() {
        return decorImg;
    }
}
