package com.code.question3;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 准备一个 HashMap 集合，统计字符串"123,456,789,123,456"中每个数字字符串出现的次数并打印出来。
 */
public class QuestionThreeTest {
    public static void main(String[] args) {
        //准备字符串信息
        String str1 = "123,456,789,123,456";
        //分割字符串信息
        String[] strings = str1.split(",");
        //通过Lambda表达式 GroupBy 语句进行对集合字符串分组，归并
        Map<String, List<String>> map = Arrays.asList(strings).stream().collect(Collectors.groupingBy(s -> s));
        //循环HashMap中信息并打印
        map.keySet().forEach(s -> System.out.println(s + "出现了" + map.get(s).size() + "次"));
    }
}
