package com.code.question5.base;

/**
 * 游戏中玩家的身份信息，农民或者地主,后续扩展用
 */
public enum Identity {
    Farmer,Landlord
}
