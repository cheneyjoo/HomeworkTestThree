package com.code.question5.base;

public class PokeCard {
    private String cardNumber;
    private Decor decor;
    private int order;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Decor getDecor() {
        return decor;
    }

    public void setDecor(Decor decor) {
        this.decor = decor;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * 重组牌信息，由花色和牌数值组成
     * @return
     */
    @Override
    public String toString() {
        return null == decor ? cardNumber : decor.getDecorImg() + cardNumber;
    }
}
