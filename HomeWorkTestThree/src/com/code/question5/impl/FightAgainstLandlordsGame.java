package com.code.question5.impl;

import com.code.question5.base.Decor;
import com.code.question5.base.Game;
import com.code.question5.base.ISands;
import com.code.question5.base.PokeCard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 斗地主游戏
 */
public class FightAgainstLandlordsGame extends Game implements ISands {
    private List<PokeCard> cards;
    private List<FightAgainstLandlordsPlayer> players;

    public List<PokeCard> getCards() {
        return cards;
    }

    /**
     * 获取所有玩家
     * @return
     */
    public List<FightAgainstLandlordsPlayer> getPlayers() {
        return players;
    }

    /**
     * 重写Set方法，每次传入一个玩家
     * @param player
     */
    public void setPlayers(FightAgainstLandlordsPlayer player) {
        if (players == null) {
            this.players = new ArrayList<>();
        }
        this.players.add(player);
    }

    /**
     * 创建游戏时初始化一副扑克牌
     */
    public FightAgainstLandlordsGame() {
        this.setName("欢乐斗地主");
        this.cards = new ArrayList<>();
        String[] cardNumbers = new String[]{"大王", "小王", "2", "A", "K", "Q", "J", "10", "9", "8", "7", "6", "5", "4", "3"};
        for (int i = 0; i < cardNumbers.length; i++) {
            if (!cardNumbers[i].equals("小王") && !cardNumbers[i].equals("大王")) {
                for (Decor decor : Decor.values()) {
                    PokeCard card = new PokeCard();
                    card.setCardNumber(cardNumbers[i]);
                    card.setOrder(i + 1);
                    card.setDecor(decor);
                    this.cards.add(card);
                }
            } else {
                PokeCard card = new PokeCard();
                card.setCardNumber(cardNumbers[i]);
                card.setOrder(i + 1);
                this.cards.add(card);
            }
        }
    }

    /**
     * 洗牌
     */
    @Override
    public void ShuffleCards() {
        System.out.println("性感荷官在线洗牌！");
        Collections.shuffle(cards);
        System.out.println("洗牌完毕！");
    }

    /**
     * 发牌
     */
    @Override
    public void DealCards() {
        if (this.players == null || this.players.size() < 3) {
            System.out.println("玩家不足，不能发牌");
            return;
        }
        System.out.println("开始发牌！！");
        for (int i = 0; i < cards.size() - 3; i = i + 3) {
            for (int j = 0; j < this.players.size(); j++) {
                this.players.get(j).setCards(cards.get(i + j));
                System.out.println(this.players.get(j).getName() + ":" + this.players.get(j).getCards());
            }
            System.out.println("--------------------------------------------------------------------");
        }
        System.out.println("发牌完毕！！");
    }

    /**
     * 查看玩家手牌与底牌方法
     */
    public void ShowPlayerCardsAndLastCards() {
        for (int i = 0; i < this.players.size(); i++) {
            this.players.get(i).ShuffleCards();
            System.out.println(this.players.get(i).getName()+":"+this.players.get(i).getCards().toString());
        }
        System.out.println("底牌为："+cards.subList(cards.size() - 3,cards.size()).toString());
    }
}
