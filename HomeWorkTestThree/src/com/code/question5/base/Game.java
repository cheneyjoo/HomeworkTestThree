package com.code.question5.base;

/**
 * 游戏抽象类
 */
public abstract class Game {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
