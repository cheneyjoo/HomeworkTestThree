package com.code.question4;

import com.code.question4.base.SearchType;
import com.code.question4.impl.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Client {
    public static void main(String[] args) throws IOException {
        //初始化学生信息
        InitStudents();
        Student student = new Student();
        System.out.println("**************************************************");
        System.out.println("*                                                *");
        System.out.println("*                拉勾学生管理系统                 *");
        System.out.println("*                                                *");
        System.out.println("**************************************************");
        //是否退出标识
        int flag = 1;
        System.out.println(student.toString());
        do {
            System.out.println("请输入要操作的功能编号：");
            System.out.println("1：新增学生信息");
            System.out.println("2：修改学生信息");
            System.out.println("3：查找学生信息");
            System.out.println("4：删除学生信息");
            System.out.println("5：全部学生信息");
            System.out.println("6：退出学生系统");
            String input = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            switch (input) {
                case "1":
                    AddStudent();
                    break;
                case "2":
                    UpdateStudent();
                    break;
                case "3":
                    FindStudents();
                    break;
                case "4":
                    DeleteStudent();
                    break;
                case "5":
                    System.out.println(student.toString());
                    break;
                default:
                    flag = 0;
                    System.out.println("谢谢使用！！");
                    break;
            }
        } while (flag == 1);
    }

    /**
     * 初始化学生
     */
    public static void InitStudents() {
        Student student = new Student("2020100301", "张飞", 30);
        student.Post(student);
        student = new Student("2020100302", "关羽", 35);
        student.Post(student);
        student = new Student("2020100303", "马超", 38);
        student.Post(student);
        student = new Student("2020100304", "赵云", 33);
        student.Post(student);
        student = new Student("2020100305", "黄忠", 50);
        student.Post(student);
    }

    /**
     * 添加学生
     */
    public static void AddStudent() throws IOException {
        //学号校验正则
        String reg = "[1-9]\\d{9}";
        String regAge = "[1-9]\\d{1}";
        String studentNo = "", studentName = "";
        int age = 0;
        System.out.println("请输入学生学号：");
        while (true) {
            studentNo = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (studentNo.matches(reg)) {
                break;
            }
            System.out.println("学号格式有误，请重新输入：");
        }
        System.out.println("请输入学生姓名：");
        studentName = (new BufferedReader(new InputStreamReader(System.in))).readLine();
        System.out.println("请输入学生年龄：");
        while (true) {
            String inputAge = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (inputAge.matches(regAge)) {
                age = Integer.parseInt(inputAge);
                break;
            }
            System.out.println("学生年龄输入有误，请重新输入：");
        }
        Student addStudent = new Student(studentNo, studentName, age);
        addStudent.Post(addStudent);
        System.out.println("添加成功！！");
        System.out.println(addStudent.toString());
    }

    /**
     * 修改学生信息
     *
     * @throws IOException
     */
    public static void UpdateStudent() throws IOException {
        //学号校验正则
        String reg = "[1-9]\\d{9}";
        String regAge = "[1-9]\\d{1}";
        String studentNo = "", studentName = "";
        int age = 0;
        System.out.println("请输入学生学号：");
        while (true) {
            studentNo = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (studentNo.matches(reg)) {
                if (Student.ExistsStudent(studentNo)) {
                    break;
                } else
                    System.out.println("该学生不存在！请重新输入：");
            } else
                System.out.println("学号格式有误，请重新输入：");
        }
        System.out.println("请输入修改的学生姓名：");
        studentName = (new BufferedReader(new InputStreamReader(System.in))).readLine();
        System.out.println("请输入修改的学生年龄：");
        while (true) {
            String inputAge = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (inputAge.matches(regAge)) {
                age = Integer.parseInt(inputAge);
                break;
            }
            System.out.println("学生年龄输入有误，请重新输入：");
        }
        Student addStudent = new Student(studentNo, studentName, age);
        addStudent.Put(addStudent);
        System.out.println("更新成功！！");
        System.out.println(addStudent.toString());
    }

    /**
     * 查找学生信息
     */
    public static void FindStudents() throws IOException {
        System.out.println("请输入要查找学生的信息类别：");
        System.out.println("1:根据学号查找");
        System.out.println("2:根据姓名查找");
        System.out.println("3:根据年龄查找");
        String input = (new BufferedReader(new InputStreamReader(System.in))).readLine();
        Student student = new Student();
        switch (input) {
            case "1":
                System.out.println("请输入学号：");
                String studentNo = (new BufferedReader(new InputStreamReader(System.in))).readLine();
                System.out.println(student.toString(student.Get(studentNo, SearchType.No)));
                break;
            case "2":
                System.out.println("请输入姓名：");
                String studentName = (new BufferedReader(new InputStreamReader(System.in))).readLine();
                System.out.println(student.toString(student.Get(studentName, SearchType.Name)));
                break;
            case "3":
                System.out.println("请输入年龄：");
                String age = (new BufferedReader(new InputStreamReader(System.in))).readLine();
                System.out.println(student.toString(student.Get(age, SearchType.Age)));
                break;
            default:
                System.out.println(student.toString());
                break;
        }
    }

    /**
     * 删除学生信息
     *
     * @throws IOException
     */
    public static void DeleteStudent() throws IOException {
        //学号校验正则
        String reg = "[1-9]\\d{9}";
        String studentNo = "";
        System.out.println("请输入学生学号：");
        while (true) {
            studentNo = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (studentNo.matches(reg)) {
                if (Student.ExistsStudent(studentNo)) {
                    break;
                } else {
                    System.out.println("该学生不存在！！");
                    return;
                }
            } else
                System.out.println("学号格式有误，请重新输入：");
        }
        Student deleteStudent = new Student();
        deleteStudent.Delete(studentNo);
        System.out.println("删除成功！！");
        System.out.println(deleteStudent.toString());
    }
}
