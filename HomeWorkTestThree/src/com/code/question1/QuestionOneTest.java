package com.code.question1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class QuestionOneTest {
    /**
     * 编程统计字符串中大写字母、小写字母、数字、其它字符的个数并打印出来。
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        //匹配非大写字母正则
        String bigLetterRegex = "[^(A-Z)]+";
        //匹配非小写字母正则
        String smallLetterRegex = "[^(a-z)]+";
        //匹配非数字正则
        String numberRegex = "[^(0-9)]+";
        //匹配非字符正则
        String otherRegex = "[^(\\W)]+";
        while (true) {
            System.out.println("请输入要匹配的字符串（输入n中断）：");
            String input = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (input.equalsIgnoreCase("n")) {
                break;
            }

            //匹配非大写字母替换为空，取全部大写字母
            String bigLetter = input.replaceAll(bigLetterRegex, "");
            System.out.println("发现大写字母" + bigLetter + ",目前大写字母已有" + bigLetter.length() + "个");
            //匹配非小写字母替换为空，取全部小写字母
            String smallLette = input.replaceAll(smallLetterRegex, "");
            System.out.println("发现小写字母" + smallLette + ",目前小写字母已有" + smallLette.length() + "个");
            //匹配非数字替换为空，取全部数字
            String number = input.replaceAll(numberRegex, "");
            System.out.println("发现数字" + number + ",目前数字已有" + number.length() + "个");
            //匹配非字符替换为空，取全部字符
            String other = input.replaceAll(otherRegex, "");
            System.out.println("发现其他字符" + other + ",目前其他字符已有" + other.length() + "个");
        }
    }
}
