package com.code.question5;

import com.code.question5.impl.FightAgainstLandlordsGame;
import com.code.question5.impl.FightAgainstLandlordsPlayer;

public class Client {
    public static void main(String[] args) {
        System.out.println("欢迎子曰来到澳门皇家赌场之斗地主专场");
        System.out.println("下面给你介绍你本场游戏的对手们！");

        System.out.println("有请有着香港赌神称号，赌场无敌手之称的周润发！！！");
        FightAgainstLandlordsPlayer zhouRunFa = new FightAgainstLandlordsPlayer("周润发", 1000000000);
        System.out.println(zhouRunFa.getName() + "这次带来了" + zhouRunFa.getMoney() + "美元的赌资");
        System.out.println("--------------------------------------------------------------");

        System.out.println("接下来有请有着香港赌侠称号，赌场魔术手之称的周星驰！！！");
        FightAgainstLandlordsPlayer zhouXingChi = new FightAgainstLandlordsPlayer("周星驰", 700000000);
        System.out.println(zhouXingChi.getName() + "这次带来了" + zhouXingChi.getMoney() + "美元的赌资");
        System.out.println("--------------------------------------------------------------");

        System.out.println("接下来隆重介绍下我们的挑战者，有着拉勾教育彦祖之称的逢赌必输的挑战者，子曰老师！！！");
        FightAgainstLandlordsPlayer ziyue = new FightAgainstLandlordsPlayer("子曰", 8000000000l);
        System.out.println(ziyue.getName() + "这次带来了" + ziyue.getMoney() + "美元的赌资");
        System.out.println("------------------------------------------------------------------");

        FightAgainstLandlordsGame game = new FightAgainstLandlordsGame();
        System.out.println(game.getName() + "游戏开始");
        System.out.println("请三位玩家入座！");
        game.setPlayers(zhouRunFa);
        game.setPlayers(zhouXingChi);
        game.setPlayers(ziyue);
        game.ShuffleCards();
        game.DealCards();
        System.out.println("--------------------------------------------------------------");
        System.out.println("查看玩家手牌与底牌：");
        game.ShowPlayerCardsAndLastCards();
    }
}
