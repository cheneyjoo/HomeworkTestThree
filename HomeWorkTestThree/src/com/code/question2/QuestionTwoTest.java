package com.code.question2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class QuestionTwoTest {
    /**
     * 编程获取两个指定字符串中的最大相同子串并打印出来。
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        while (true) {
            System.out.println("输入第一个字符串（输入n中断）：");
            String input1 = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (input1.equalsIgnoreCase("n")) {
                break;
            }

            System.out.println("输入第二个字符串（输入n中断）：");
            String input2 = (new BufferedReader(new InputStreamReader(System.in))).readLine();
            if (input2.equalsIgnoreCase("n")) {
                break;
            }
            //求出两个字符串的长串
            String longStr = input1.length() >= input2.length() ? input1 : input2;
            //求出两个字符串的短串
            String shortStr = input1.length() >= input2.length() ? input2 : input1;
            //定义HashMap存储长度与字符串对应关系
            Map<Integer, String> strLenMap = new HashMap<>();
            //遍历短串的长度
            for (int i = 0; i < shortStr.length(); i++) {
                //从短串开始到末端依次截取，判断是否存在与长串中，存在即保存在HashMap中，以长度为Key，字符串内容为Value，后续根据Key进行判断最长字符串
                for (int j = shortStr.length(); j > i; j--) {
                    if (longStr.contains(shortStr.substring(i, j))) {
                        strLenMap.put(shortStr.substring(i, j).length(), shortStr.substring(i, j));
                    }
                }
            }
            //取Map所有Key值的Set集合
            Set<Integer> strLenKeys = strLenMap.keySet();
            //取最大的Key值
            Integer maxKey = Collections.max(strLenKeys);
            //取对应最大长度Key的Value值
            String value = strLenMap.get(maxKey);
            System.out.println("两个字符串最大长度相同子串为：" + value);
        }
    }
}
